import axios from 'axios'

const useLocalhost = location.hostname === 'localhost' && true  //  cambiare a false se si vuole usare l'api remota anche da locale

console.log('Use localhost: ' + useLocalhost)

const baseURL = useLocalhost ? 'http://localhost:8000/api/v1' : 'https://api.g17.michaelsaccone.it/api/v1';

const instance = axios.create({
    baseURL
})

export default instance;
