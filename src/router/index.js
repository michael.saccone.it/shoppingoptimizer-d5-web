import Vue from 'vue'
import { store } from '@/store'
import VueRouter from 'vue-router'
import Registration from "@/pages/Registration";
import Login from "@/pages/Login";
import Home from "@/pages/Home";
import Supermarkets from "@/pages/Supermarkets";
import Users from "@/pages/Users";
import Categories from "@/pages/Categories";
import Products from "@/pages/Products"
import Cart from "@/pages/Cart";

Vue.use(VueRouter)

function beforeEnter (to, from, next) {
    if (store.getters.isLogged) {
        next()
    } else {
        next('/login')
    }
}

const routes = [
    {
        path: '/',
        component: Home
    },
    {
        path: '/registration',
        component: Registration
    },
    {
        path: '/login',
        component: Login
    },
    {
        path: '/supermarkets',
        component: Supermarkets,
        beforeEnter
    },
    {
        path: '/users',
        component: Users,
        beforeEnter
    },
    {
        path: '/categories',
        component: Categories,
        beforeEnter
    },
    {
        path: '/cart',
        component: Cart,
        beforeEnter
    },
    {
        path: '/products',
        component: Products,
        beforeEnter
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
