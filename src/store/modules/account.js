import Vue from 'vue'
import router from '../../router/index'

const state = {
    id: null,
    name: null,
    surname: null,
    email: null,
    admin: null
}

const getters = {
    isLogged(state) {
        return state.id !== null
    },
    id: (state) => state.id,
    name: (state) => state.name,
    surname: (state) => state.surname,
    email: (state) => state.email,
    isAdmin: (state) => state.admin
}

const mutations = {
    loginUser(state, userData) {
        Object.assign(state, userData)
        router.push('/');
    },
    logoutUser(state) {
        Object.keys(state).forEach(k => {
            state[k] = null
        })
        router.push('/');
    }
}

const actions = {
    login({commit}, authData) {
        Vue.prototype.$http.post('/auth/login', authData)
            .then((response) => {
                commit('loginUser', response.data)
            })
            .catch(() => {
                window.alert("Credenziali errate!")
            });

    }
}

export default {
    state,
    getters,
    mutations,
    actions
}
