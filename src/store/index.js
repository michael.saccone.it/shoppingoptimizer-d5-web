import Vue from 'vue'
import Vuex from 'vuex'

import account from './modules/account'

import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export const store = new Vuex.Store({
    plugins: [
        createPersistedState({
            paths: ['account']
        })
    ],
    state: {},
    mutations: {},
    actions: {},
    getters: {},
    modules: {
        account
    }
})

