# Shop&Go - the 30L project

## Istruzioni per l'utilizzo

## link applicazione online

[Frontend web](https://g17.michaelsaccone.it)

[https://g17.michaelsaccone.it](https://g17.michaelsaccone.it).

##Se proprio vuole eseguirlo in locale:

### Setup progetto
```
npm install
```

### Compilazione ed esecuzione con node
```
npm run serve
```

### Compilazione ed esecuzione per la produzione
```
npm run build
```

### Lints e fix file
```
npm run lint
```

### Mettere 30L su Esse3 usando python o postman
```
from unie3api.unie3api import uniE3Api

e3 = uniE3Api('https://unical.esse3.cineca.it/e3rest/api', 'TUOTOKEN')
# or
e3 = uniE3Api('https://unical.esse3.cineca.it/e3rest/api',
              username='thatuser', password='thatpass')


# usiamo sempre il codice fiscale come identificativo
e3.carriera('CODICEFISCALEQUI')
```
